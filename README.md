TDD assignment from Chas Academy DevOps course

To try it out
1. Clone the repository
2. Create a virtual environment:
   ```$ virtualenv env```
3. Activate environment:
   ```$ source env/bin/activate``` 
4. Install requirements:
   ```$ pip install -r requirements.txt```
5. Run from src/main.py

Test built in Pytest.
```$ pytest test/```

TDD applied in year 2020, found in src/twenty
BDD applied in year 2018, found in src/eighteen