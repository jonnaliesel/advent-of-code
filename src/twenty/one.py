"""Advent of code, 2020, day one"""
import math

def multiplied_list(data):
    return math.prod(data)
    
def find_2020_in_two(puzzle_input):
    """Given a list of numbers, find the two that sums up to 2020"""
    num1 = 0
    num2 = 0
    for number in enumerate(puzzle_input):
        num1 = int(number[1])
        for index, number in enumerate(puzzle_input):
            if index + 1 < len(puzzle_input):
                num2 = int(puzzle_input[index + 1])
                if num1 + num2 == 2020:
                    return [num1, num2]

def find_2020_in_three(puzzle_input):
    """Given a list of numbers, find the two that sums up to 2020"""
    num1 = 0
    num2 = 0
    num3 = 0
    for number in enumerate(puzzle_input):
        num1 = int(number[1])
        for index, number in enumerate(puzzle_input):
            if index + 1 < len(puzzle_input):
                num2 = int(puzzle_input[index + 1])

            for index, number in enumerate(puzzle_input):
                if index + 2 < len(puzzle_input):
                    num3 = int(puzzle_input[index + 1])
                    if num1 + num2 + num3 == 2020:
                        return [num1, num2, num3]
