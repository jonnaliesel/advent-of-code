"""Advent of code, 2020, day two"""
import re

def format_data(puzzle_input):
    formatted_data = []
    for item in puzzle_input:
        formatted_string = re.split('[-|: ]', item)
        formatted_data.append({"min": int(formatted_string[0]), "max": int(formatted_string[1]), "letter": formatted_string[2], "password": formatted_string[4]})
    return formatted_data

def check_instances(password):
    count = password["password"].count(password["letter"])
    if password["min"] <= count <= password["max"]:
        return 'Valid'
    else:
        return 'Not valid'

def check_positions(input):
    password = input["password"]
    letter = input["letter"]
    pos1 = int(input["min"]) - 1
    pos2 = int(input["max"]) -1
    
    if password[pos1] == letter and password[pos2] == letter:
        return 'Not valid'
    elif password[pos1] == letter or password[pos2] == letter:
        return 'Valid'
    else:
        return 'Not valid'

def valid_passwords_one(puzzle_input):
    valid_passwords = []
    for password in puzzle_input:
        if check_instances(password) == 'Valid':
            valid_passwords.append(password)
    return len(valid_passwords)

def valid_passwords_two(puzzle_input):
    valid_passwords = []
    for password in puzzle_input:
        if check_positions(password) == 'Valid':
            valid_passwords.append(password)
    return len(valid_passwords)
