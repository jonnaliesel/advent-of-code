"""Main loop for all puzzels"""

from twenty.one import find_2020_in_two, find_2020_in_three, multiplied_list
from twenty.two import check_instances, valid_passwords_one, format_data, valid_passwords_two

def get_puzzle_input(year, day):
    """TO DO: Use with instead of open"""
    puzzle_input = []
    for line in open(f'src/{year}/{day}.txt', 'r').readlines():
        puzzle_input.append(line.strip())
    return puzzle_input

while True:
    puzzle_input = get_puzzle_input('twenty', 'one')
    puzzle_output = find_2020_in_two(puzzle_input)
    print('**************DAY 1, PART 1 **************')
    print(f"Numbers: {puzzle_output}")
    print(f"Multiplied: {multiplied_list(puzzle_output)}")

    puzzle_output = find_2020_in_three(puzzle_input)
    print('************** DAY 1, PART 2 **************')
    print(f"Numbers: {puzzle_output}")
    print(f"Multiplied: {multiplied_list(puzzle_output)}")
    
    puzzle_input = get_puzzle_input('twenty', 'two')
    formatted_input = format_data(puzzle_input)
    print('\n\n************** DAY 2, PART 1 **************')
    print(f"Valid passwords: {valid_passwords_one(formatted_input)}")
    print('************** DAY 2, PART 2 **************')
    print(f"Valid passwords: {valid_passwords_two(formatted_input)}")
    
    quit()
