"""Unit tests for Advent of code, 2020"""
import pytest
from src.twenty.one import find_2020_in_two, find_2020_in_three
from src.twenty.two import check_instances, valid_passwords_one, format_data, check_positions, valid_passwords_two

def test_day_one_part_one():
    """Given a list of numbers, find the two that sums up to 2020"""
    puzzle_input = [1721, 979, 366, 299, 675, 1456]
    puzzle_output = find_2020_in_two(puzzle_input)

    assert len(puzzle_output) is 2
    assert sum(puzzle_output) == 2020


def test_day_one_part_two():
    """Given a list of numbers, find the two that sums up to 2020"""
    puzzle_input = [1721, 979, 366, 299, 675, 1456]
    puzzle_output = find_2020_in_three(puzzle_input)

    assert len(puzzle_output) is 3
    assert sum(puzzle_output) == 2020

#Tests day two
@pytest.fixture
def puzzle_input_day_two():
    input = [
        {"min": 1, "max": 3, "letter":'a', "password": 'abcde'},
        {"min": 1, "max": 3, "letter": 'b', "password": 'cdefg'},
        {"min": 2, "max": 9, "letter": 'c', "password": 'ccccccccc'}
    ]
    return input

def test_input_formatting():
    result = format_data(['2-9 c: ccccccccc'])
    test = [{"min": 2, "max": 9, "letter": 'c', "password": 'ccccccccc'}]

    assert result == test
    
def test_day_two_puzzle_input(puzzle_input_day_two):
    puzzle_input = puzzle_input_day_two
    assert len(puzzle_input) == 3

    for index, item in enumerate(puzzle_input):
        min_instances = puzzle_input[index]["min"]
        max_instances = puzzle_input[index]["max"]
        letter = puzzle_input[index]["letter"]
        password = puzzle_input[index]["password"]

        assert type(min_instances) and type(max_instances) is int
        assert type(letter) and type(password) is str

def test_valid_password(puzzle_input_day_two):
    assert check_instances(puzzle_input_day_two[0]) is 'Valid'
    assert check_positions(puzzle_input_day_two[0]) is 'Valid'

def test_not_valid_password(puzzle_input_day_two):
    assert check_instances(puzzle_input_day_two[1]) == 'Not valid'
    assert check_positions(puzzle_input_day_two[1]) == 'Not valid'


def test_number_of_valid_passwords(puzzle_input_day_two):
    assert valid_passwords_one(puzzle_input_day_two) == 2
    assert valid_passwords_two(puzzle_input_day_two) == 1
